use std::{
    collections::{BTreeMap, HashMap, VecDeque},
    fmt::Debug,
};

use tracing::debug;

#[cfg(test)]
mod test;
pub struct Description {
    pub inputs: Vec<(String, bool)>,
    pub outputs: Vec<(String, bool)>,
    pub name: String,
}

pub trait CircuitFactory: Debug {
    fn produce(&self) -> Box<dyn Circuit>;

    fn clone(&self) -> Box<dyn CircuitFactory>;
}

#[derive(Debug)]
pub enum SimulationStep {
    Connection(Address, Address),
    Component(usize),
}

pub trait Circuit: Debug {
    fn set_input(&mut self, index: String, value: bool);
    fn input(&self, index: &String) -> bool;
    fn output(&self, index: &String) -> bool;
    fn simulate_step(&mut self) -> Option<SimulationStep>;
    fn apply(&mut self, step: SimulationStep) -> Option<String>;

    fn simulate(&mut self) -> Vec<String> {
        let mut ret = Vec::new();

        while let Some(step) = self.simulate_step() {
            if let Some(output) = self.apply(step) {
                ret.push(output);
            }
        }

        ret
    }

    fn describe(&self) -> Description;
}

#[derive(Debug, Hash, PartialEq, Eq, PartialOrd, Ord, Clone)]
pub enum Address {
    IO(String),
    Circuit(usize, String),
}

pub struct Composit {
    circuits: HashMap<usize, Box<dyn Circuit>>,
    connections: HashMap<Address, Vec<Address>>,
    inputs: BTreeMap<String, bool>,
    outputs: BTreeMap<String, bool>,
    name: String,
    simulation_steps: VecDeque<SimulationStep>,
    next_index: usize,
}

impl std::fmt::Debug for Composit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Simulation")
            .field("connections", &self.connections)
            .field("inputs", &self.inputs)
            .field("outputs", &self.outputs)
            .field("name", &self.name)
            .field("simulation_steps", &self.simulation_steps)
            .finish()
    }
}

impl Composit {
    fn initialize(&mut self) {
        let mut starts = Vec::new();
        for (index, _) in self.inputs.iter() {
            let start = Address::IO(index.clone());
            starts.push(start);
        }

        for start in starts {
            self.trigger_update(&start);
        }

        self.simulate();
    }

    pub fn inputs(&self) -> &BTreeMap<String, bool> {
        &self.inputs
    }

    pub fn outputs(&self) -> &BTreeMap<String, bool> {
        &self.outputs
    }

    pub fn component(&self, index: &usize) -> Option<&Box<dyn Circuit>> {
        self.circuits.get(index)
    }

    pub fn components(&self) -> impl Iterator<Item = (&usize, &Box<dyn Circuit>)> {
        self.circuits.iter()
    }

    fn get_input_value(&self, from: &Address) -> bool {
        match from {
            Address::IO(name) => self.inputs.get(name).cloned().unwrap_or_default(),
            Address::Circuit(index, name) => {
                if let Some(c) = self.circuits.get(index) {
                    c.output(name)
                } else {
                    false
                }
            }
        }
    }

    fn trigger_update(&mut self, start: &Address) {
        if let Some(connections) = self.connections.get(start) {
            for c in connections {
                self.simulation_steps
                    .push_back(SimulationStep::Connection(start.clone(), c.clone()))
            }
        }
    }

    pub fn add_component(&mut self, component: Box<dyn Circuit>) {
        self.circuits.insert(self.next_index, component);
        self.simulation_steps
            .push_back(SimulationStep::Component(self.next_index));
        self.next_index += 1;
    }

    pub fn add_connection(&mut self, from: Address, to: Address) {
        self.trigger_update(&from);
        match self.connections.get_mut(&from) {
            Some(targets) => targets.push(to),
            None => {
                self.connections.insert(from, vec![to]);
            }
        }
    }
}

impl Circuit for Composit {
    fn set_input(&mut self, index: String, value: bool) {
        match self.inputs.get_mut(&index) {
            Some(old) if *old != value => {
                *old = value;

                let start = Address::IO(index);
                self.trigger_update(&start);
            }
            _ => {}
        }
    }

    fn output(&self, index: &String) -> bool {
        self.outputs.get(index).cloned().unwrap_or_default()
    }

    fn simulate_step(&mut self) -> Option<SimulationStep> {
        self.simulation_steps.pop_front()
    }

    fn describe(&self) -> Description {
        Description {
            inputs: self
                .inputs
                .iter()
                .map(|(name, val)| (name.clone(), *val))
                .collect(),
            outputs: self
                .outputs
                .iter()
                .map(|(name, val)| (name.clone(), *val))
                .collect(),
            name: self.name.clone(),
        }
    }

    fn apply(&mut self, step: SimulationStep) -> Option<String> {
        debug!(?step, ?self.name);

        match step {
            SimulationStep::Connection(from, to) => {
                let value = self.get_input_value(&from);

                match to {
                    Address::IO(to) => {
                        debug!(value, "Setting output");
                        self.outputs.insert(to.clone(), value);
                        return Some(to);
                    }
                    Address::Circuit(index, name) => {
                        if let Some(c) = self.circuits.get_mut(&index) {
                            c.set_input(name.clone(), value);
                            self.simulation_steps
                                .push_back(SimulationStep::Component(index));
                        }

                        return None;
                    }
                }
            }
            SimulationStep::Component(index) => {
                if let Some(c) = self.circuits.get_mut(&index) {
                    let changes = c.simulate();

                    for start in changes {
                        self.trigger_update(&Address::Circuit(index, start));
                    }
                }

                None
            }
        }
    }

    fn input(&self, index: &String) -> bool {
        self.inputs.get(index).cloned().unwrap_or_default()
    }
}

#[derive(Debug)]
pub struct CompositFactory {
    pub circuits: Vec<Box<dyn CircuitFactory>>,
    pub connections: HashMap<Address, Vec<Address>>,
    pub inputs: Vec<String>,
    pub outputs: Vec<String>,
    pub name: String,
}

impl Clone for CompositFactory {
    fn clone(&self) -> Self {
        Self {
            circuits: self.circuits.iter().map(|c| c.as_ref().clone()).collect(),
            connections: self.connections.clone(),
            inputs: self.inputs.clone(),
            outputs: self.outputs.clone(),
            name: self.name.clone(),
        }
    }
}

impl CompositFactory {
    pub fn produce_composit(&self) -> Composit {
        let mut s = Composit {
            circuits: self
                .circuits
                .iter()
                .enumerate()
                .map(|(i, f)| (i, f.produce()))
                .collect(),
            connections: self.connections.clone(),
            inputs: self.inputs.iter().cloned().map(|i| (i, false)).collect(),
            name: self.name.clone(),
            outputs: self.outputs.iter().cloned().map(|i| (i, false)).collect(),
            simulation_steps: VecDeque::new(),
            next_index: 0,
        };

        s.initialize();

        s
    }
}

impl CircuitFactory for CompositFactory {
    fn produce(&self) -> Box<dyn Circuit> {
        Box::new(self.produce_composit())
    }

    fn clone(&self) -> Box<dyn CircuitFactory> {
        Box::new(Clone::clone(self))
    }
}
