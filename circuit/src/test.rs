use super::*;

#[derive(Debug, Default)]
struct And {
    a: bool,
    b: bool,
    simulation_steps: VecDeque<SimulationStep>,
}

impl Circuit for And {
    fn set_input(&mut self, index: String, value: bool) {
        match index.as_str() {
            "a" => {
                self.a = value;
                self.simulation_steps.push_back(SimulationStep::Connection(
                    Address::IO("a".to_string()),
                    Address::IO("output".to_string()),
                ));
            }
            "b" => {
                self.b = value;
                self.simulation_steps.push_back(SimulationStep::Connection(
                    Address::IO("b".to_string()),
                    Address::IO("output".to_string()),
                ));
            }
            _ => {}
        }
    }

    fn output(&self, index: &String) -> bool {
        match index.as_str() {
            "output" => self.a && self.b,
            _ => false,
        }
    }

    fn simulate_step(&mut self) -> Option<SimulationStep> {
        self.simulation_steps.pop_front()
    }

    fn describe(&self) -> Description {
        Description {
            inputs: vec![("a".to_string(), self.a), ("b".to_string(), self.b)],
            outputs: vec![("output".to_string(), self.a && self.b)],
            name: "And".to_string(),
        }
    }

    fn apply(&mut self, step: SimulationStep) -> Option<String> {
        if let SimulationStep::Connection(_, Address::IO(name)) = step {
            Some(name)
        } else {
            None
        }
    }

    fn input(&self, index: &String) -> bool {
        match index.as_str() {
            "a" => self.a,
            "b" => self.b,
            _ => false,
        }
    }
}

#[derive(Debug, Default)]
struct Not {
    a: bool,
    simulation_steps: VecDeque<SimulationStep>,
}

impl Circuit for Not {
    fn set_input(&mut self, index: String, value: bool) {
        match index.as_str() {
            "a" => {
                self.a = value;
                self.simulation_steps.push_back(SimulationStep::Connection(
                    Address::IO("a".to_string()),
                    Address::IO("output".to_string()),
                ));
            }
            _ => {}
        }
    }

    fn output(&self, index: &String) -> bool {
        match index.as_str() {
            "output" => !self.a,
            _ => false,
        }
    }

    fn simulate_step(&mut self) -> Option<SimulationStep> {
        self.simulation_steps.pop_front()
    }

    fn describe(&self) -> Description {
        Description {
            inputs: vec![("a".to_string(), self.a)],
            outputs: vec![("output".to_string(), !self.a)],
            name: "Not".to_string(),
        }
    }

    fn apply(&mut self, step: SimulationStep) -> Option<String> {
        if let SimulationStep::Connection(_, Address::IO(name)) = step {
            Some(name)
        } else {
            None
        }
    }

    fn input(&self, index: &String) -> bool {
        match index.as_str() {
            "a" => self.a,
            _ => false,
        }
    }
}

#[test]
pub fn or() {
    let and = And::default();
    let not_a = Not::default();
    let not_b = Not::default();
    let not_output = Not::default();

    let mut connections = HashMap::new();
    connections.insert(
        Address::IO("a".to_string()),
        vec![Address::Circuit(1, "a".to_string())],
    );
    connections.insert(
        Address::IO("b".to_string()),
        vec![Address::Circuit(2, "a".to_string())],
    );
    connections.insert(
        Address::Circuit(1, "output".to_string()),
        vec![Address::Circuit(0, "a".to_string())],
    );
    connections.insert(
        Address::Circuit(2, "output".to_string()),
        vec![Address::Circuit(0, "b".to_string())],
    );
    connections.insert(
        Address::Circuit(0, "output".to_string()),
        vec![Address::Circuit(3, "a".to_string())],
    );
    connections.insert(
        Address::Circuit(3, "output".to_string()),
        vec![Address::IO("output".to_string())],
    );

    let mut circuits: HashMap<usize, Box<dyn Circuit>> = HashMap::new();

    circuits.insert(0, Box::new(and));
    circuits.insert(1, Box::new(not_a));
    circuits.insert(2, Box::new(not_b));
    circuits.insert(3, Box::new(not_output));

    let mut or = Composit {
        circuits,
        connections,
        inputs: BTreeMap::new(),
        outputs: BTreeMap::new(),
        name: "Or".to_string(),
        simulation_steps: VecDeque::new(),
        next_index: 0
    };

    or.set_input("a".to_string(), true);
    or.set_input("b".to_string(), true);

    or.simulate();

    assert!(or.output(&"output".to_string()));

    panic!();
}
