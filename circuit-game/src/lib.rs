use states::GameState;
use wankel_engine::{prelude::*, Color, Context, Handle};

mod states;

struct Game {
    state: GameState,

    menu: Option<states::menu::Menu>,
    playing: Option<states::playing::Playing>,

    globals: GameGlobals,
}

struct GameGlobals {
    font: Handle<Font>,
    clock_timer: f32,
}

impl Game {
    fn new(ctx: &mut Context) -> Self {
        let font = ctx.load_asset("data/fonts/main.ttf");

        Self {
            state: GameState::Menu,
            menu: None,
            playing: None,
            globals: GameGlobals {
                font,
                clock_timer: 1.0,
            },
        }
    }
}

impl State for Game {
    fn init(ctx: &mut wankel_engine::Context) -> Self {
        ctx.background_color(Color {
            r: 1.0,
            g: 0.5,
            b: 0.2,
            a: 1.0,
        });
        Game::new(ctx)
    }

    fn update(
        &mut self,
        ctx: &mut wankel_engine::Context,
        d_time: f32,
        io: &mut wankel_engine::IO,
    ) {
        if let Some(new_state) = states::update(self, ctx, d_time, io) {
            self.state = new_state;
        }
    }
}

#[main]
async fn main() {
    wankel_engine::run::<Game>(Config {
        size: Size::new(960, 540),
        title: "Circuit".to_string(),
    }).await
}
