use wankel_engine::{layer::text::prelude::*, prelude::*};

use super::GameState;

#[derive(Clone)]
pub(crate) struct Menu {
    buttons: Vec<Button>,
    atlas_layer: Handle<TextureAtlasLayer>,
    text_layer: Handle<TextLayer>,
}

#[derive(Debug, Clone)]
struct Button {
    name: String,
    rect: Rect<f32>,
    target: GameState,
}

impl Menu {
    pub(crate) fn new(globals: &mut crate::GameGlobals, ctx: &mut wankel_engine::Context) -> Self {
        let tl = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/tl.png") };

        let t = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/t.png") };

        let tr = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/tr.png") };

        let l = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/l.png") };

        let c = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/c.png") };

        let r = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/r.png") };

        let bl = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/bl.png") };

        let b = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/b.png") };

        let br = { ctx.load_asset::<DynamicImage>("data/textures/menu/button/br.png") };

        let mut builder = UniformTextureAtlas::builder((32, 32));

        builder
            .add_texture(tl)
            .add_texture(t)
            .add_texture(tr)
            .add_texture(l)
            .add_texture(c)
            .add_texture(r)
            .add_texture(bl)
            .add_texture(b)
            .add_texture(br);

        let atlas = ctx.create_atlas(builder);

        let atlas_layer = ctx.create_layer::<TextureAtlasLayer>(atlas);
        let text_layer = ctx.create_layer::<TextLayer>(());

        let buttons = vec![
            Button {
                name: "Play".to_string(),
                rect: Rect::new(0.0, 0.0, 200.0, 100.0),
                target: GameState::Playing,
            },
            Button {
                name: "Settings".to_string(),
                rect: Rect::new(0.0, 100.0, 200.0, 100.0),
                target: GameState::Settings,
            },
        ];

        let button_nine_slice = NineSlice::new([[tl, t, tr], [l, c, r], [bl, b, br]], 32.0);

        atlas_layer.update(ctx, |layer, _ctx| {
            for b in &buttons {
                layer.render_nine_slice(
                    &button_nine_slice,
                    b.rect,
                    TextureOptions {
                        tint_color: Some(Color::RED),
                        ..Default::default()
                    },
                );
            }
        }).unwrap();

        text_layer.update(ctx, |layer, _ctx| {
            for b in &buttons {
                layer.add_section(
                    TextSection::new()
                        .with_position(Point::new(b.rect.x + (b.rect.width / 2.0), b.rect.y + 10.0))
                        .with_bounds(Size::new(b.rect.width - 20.0, b.rect.height - 20.0))
                        .with_text(Text::new(b.name.clone(), globals.font).with_scale(60.0))
                        .with_h_aligh(wankel_engine::HorizontalAlign::Center),
                );
            }
        }).unwrap();


        Self {
            buttons,
            atlas_layer,
            text_layer,
        }
    }

    pub(crate) fn update(
        &mut self,
        _globals: &mut crate::GameGlobals,
        ctx: &mut wankel_engine::Context,
        _d_time: f32,
        io: &mut wankel_engine::IO,
    ) -> Option<super::GameState> {
        for b in &self.buttons {
            if let Some((x, y)) = io.input().mouse() {
                if io.input().mouse_pressed(0) {
                    let pos = Point::new(x, y);

                    if b.rect.contains(&pos) {
                        return Some(b.target.clone());
                    }
                }
            }
        }

        ctx.push_layer(Some(0.1), self.atlas_layer);
        ctx.push_layer(Some(0.05), self.text_layer);

        None
    }
}
