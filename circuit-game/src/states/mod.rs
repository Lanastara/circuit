use wankel_engine::Context;

pub(crate) mod menu;
pub(crate) mod playing;
pub(crate) mod settings;

pub(crate) fn update(
    game: &mut super::Game,
    ctx: &mut Context,
    d_time: f32,
    io: &mut wankel_engine::IO,
) -> Option<GameState> {
    match game.state {
        GameState::Menu => match &mut game.menu {
            Some(menu) => menu.update(&mut game.globals, ctx, d_time, io),
            None => {
                let mut menu = menu::Menu::new(&mut game.globals, ctx);
                let ret = menu.update(&mut game.globals, ctx, d_time, io);
                game.menu = Some(menu);
                ret
            }
        },
        GameState::Settings => settings::update(game, ctx, d_time, io),
        GameState::Playing => match &mut game.playing {
            Some(playing) => playing.update(&mut game.globals, ctx, d_time, io),
            None => {
                let mut playing = playing::Playing::new(ctx);

                let ret = playing.update(&mut game.globals, ctx, d_time, io);
                game.playing = Some(playing);
                ret
            }
        },
    }
}

#[derive(Debug, Clone)]
pub(crate) enum GameState {
    Menu,
    Settings,
    Playing,
}
