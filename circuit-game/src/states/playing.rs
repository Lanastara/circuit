use std::collections::{BTreeMap, HashMap, VecDeque};
use std::rc::Rc;

use circuit::{
    Address, Circuit, CircuitFactory, Composit, CompositFactory, Description, SimulationStep,
};
use wankel_engine::layer::texture_atlas;
use wankel_engine::texture::Texture;
use wankel_engine::{
    layer::text::prelude::*, layer::texture_atlas::LayerTexture, prelude::*, tracing::debug,
    Context,
};
use wankel_engine_egui::egui::Vec2;

use self::connection::ConnectionDirection;
use self::{
    component::Component,
    connection::{Connection, ConnectionTextures},
};

mod component;
mod connection;

const SCALE: f32 = 32.0;

#[derive(Debug, Clone)]
pub(crate) struct Textures {
    connections: ConnectionTextures,
    io: Handle<DynamicImage>,
    component: NineSlice,
    white: Handle<DynamicImage>,
}

impl Textures {
    pub fn load(ctx: &mut wankel_engine::Context, base_path: &str) -> Self {
        let white = ctx.load_asset::<DynamicImage>(&format!("{}/component/1_1.png", base_path));

        Self {
            connections: ConnectionTextures::load(ctx, base_path),
            io: ctx.load_asset::<DynamicImage>(&format!("{}/io.png", base_path)),
            component: NineSlice::new(
                [
                    [
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/0_0.png", base_path)),
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/0_1.png", base_path)),
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/0_2.png", base_path)),
                    ],
                    [
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/1_0.png", base_path)),
                        white.clone(),
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/1_2.png", base_path)),
                    ],
                    [
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/2_0.png", base_path)),
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/2_1.png", base_path)),
                        ctx.load_asset::<DynamicImage>(&format!("{}/component/2_2.png", base_path)),
                    ],
                ],
                32.0,
            ),
            white,
        }
    }

    pub fn create_texture_atlas(&self, ctx: &mut wankel_engine::Context) -> Handle<Atlas> {
        let mut builder = UniformTextureAtlas::builder((32, 32));

        builder.add_texture(self.io);

        self.component.fill_texture_atlas(&mut builder);

        self.connections.add_textures(&mut builder);

        ctx.create_atlas(builder)
    }
}

#[derive(Debug, Eq)]
enum Selection {
    Component(String),
    Connection(Option<((u8, u8), Address)>),
    Input,
    Output,
    Existing(),
}

impl PartialEq for Selection {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Selection::Component(s1), Selection::Component(s2)) if s1 == s2 => true,

            (Selection::Connection(_), Selection::Connection(_)) => true,
            (Selection::Input, Selection::Input) => true,
            (Selection::Output, Selection::Output) => true,
            (Selection::Existing(), Selection::Existing()) => true,
            _ => false,
        }
    }
}

#[derive(Debug)]
struct EguiTextures {
    io_in: wankel_engine::Handle<Texture>,
    io_out: wankel_engine::Handle<Texture>,
    connection: wankel_engine::Handle<Texture>,
}

#[derive(Debug)]
enum Element {
    Input(String),
    Output(String),
    Connection(Address),
    Component(usize),
}

#[derive(Debug)]
pub(crate) struct Playing {
    textures: Textures,
    inputs: HashMap<String, (Handle<LayerTexture, (bool, usize)>, Option<f32>)>,
    outputs: HashMap<String, Handle<LayerTexture, (bool, usize)>>,
    connections: HashMap<Address, Connection>,
    components: HashMap<usize, Component>,
    data: circuit::Composit,
    selection: std::sync::Arc<std::sync::RwLock<Option<Selection>>>,
    factories: BTreeMap<String, Box<dyn CircuitFactory>>,
    egui_textures: EguiTextures,
    element_lookup: HashMap<(usize, usize), Element>,
    factory: CompositFactory,
    ui: Handle<wankel_engine_egui::EguiLayer>,
    text_layer: Handle<TextLayer>,
    mouseover_text: Handle<TextSection>,
    texture_layer: Handle<TextureAtlasLayer>,
    mouseover_text_backdrop: Handle<LayerTexture, (bool, usize)>,
}

impl Playing {
    pub(crate) fn new(ctx: &mut wankel_engine::Context) -> Self {
        let textures = Textures::load(ctx, "data/textures/playing/");

        let atlas = textures.create_texture_atlas(ctx);

        let components = HashMap::new();

        let connections = HashMap::new();

        let ui_connections = HashMap::new();

        let factory = CompositFactory {
            circuits: vec![],
            connections,
            inputs: vec![],
            outputs: vec![],
            name: String::new(),
        };

        let data = factory.produce_composit();

        let inputs = HashMap::new();

        let outputs = HashMap::new();

        let mut factories: BTreeMap<String, Box<dyn CircuitFactory>> = BTreeMap::new();

        factories.insert("And".to_string(), Box::new(AndFactory));
        factories.insert("Not".to_string(), Box::new(NotFactory));

        let cache = wankel_engine_egui::EguiLayerCache::default();

        let in_img: Handle<DynamicImage> = ctx.load_asset("data/textures/playing/io_in.png");
        let in_texture = ctx.upload_texture(&in_img);

        let out_img: Handle<DynamicImage> = ctx.load_asset("data/textures/playing/io_out.png");
        let out_texture = ctx.upload_texture(&out_img);

        let connection_img: Handle<DynamicImage> =
            ctx.load_asset("data/textures/playing/connection.png");
        let connection_texture = ctx.upload_texture(&connection_img);

        let egui_textures = EguiTextures {
            io_in: in_texture,
            io_out: out_texture,
            connection: connection_texture,
        };

        cache.queue_user_texture(in_texture);
        cache.queue_user_texture(out_texture);
        cache.queue_user_texture(connection_texture);

        let ui = ctx.create_layer::<wankel_engine_egui::EguiLayer>(Rc::new(cache));

        let text_layer = ctx.create_layer(());

        let texture_layer = ctx.create_layer(atlas);

        let mouseover_text_backdrop =
            Handle::<TextureAtlasLayer>::update(&texture_layer, ctx, |layer, _u| {
                layer.render_texture(
                    textures.white,
                    Rect::new(0., 0., 0., 0.),
                    TextureOptions {
                        disabled: true,
                        ..Default::default()
                    },
                )
            })
            .unwrap();

        let mouseover_text =
            Handle::<TextLayer>::update(&text_layer, ctx, |l: &mut TextLayer, _u| {
                l.add_section(TextSection::new())
            })
            .unwrap();

        Self {
            textures,
            connections: ui_connections,
            components,
            data,
            inputs,
            outputs,
            selection: std::sync::Arc::new(std::sync::RwLock::new(None)),
            factories,
            factory,
            egui_textures,
            element_lookup: HashMap::new(),
            ui,
            text_layer,
            texture_layer,
            mouseover_text,
            mouseover_text_backdrop,
        }
    }

    pub(crate) fn update(
        &mut self,
        globals: &mut crate::GameGlobals,
        mut ctx: &mut wankel_engine::Context,
        d_time: f32,
        io: &mut wankel_engine::IO,
    ) -> Option<super::GameState> {
        let textures = &self.egui_textures;

        let size = ctx.size();

        let state = self.selection.clone();

        let mouse_used = self.ui.update(ctx,  |ui, _ctx| {
            if let (Some(io_in), Some(io_out), Some(connection)) = (
                ui.get_texture_id(&textures.io_in),
                ui.get_texture_id(&textures.io_out),
                ui.get_texture_id(&textures.connection),
            ) {
                ui.run(
                    |ctx| {
                        let mut lock = state.write().unwrap();
                        let v: &mut Option<Selection> = &mut lock;
                        wankel_engine_egui::egui::TopBottomPanel::bottom("ui").show(
                            ctx,
                            |ui| {
                                ui.horizontal(|ui| {
                                    ui.vertical(|ui| {
                                        let button = wankel_engine_egui::egui::ImageButton::new(
                                            io_in,
                                            wankel_engine_egui::egui::Vec2::new(32.0, 32.0),
                                        )
                                        .selected(v == &Some(Selection::Input));
                                        if ui
                                            .add(button)
                                            .on_hover_text_at_pointer("Input")
                                            .clicked()
                                        {
                                            *v = Some(Selection::Input);
                                        }

                                        let button = wankel_engine_egui::egui::ImageButton::new(
                                            io_out,
                                            wankel_engine_egui::egui::Vec2::new(32.0, 32.0),
                                        )
                                        .selected(v == &Some(Selection::Output));
                                        if ui
                                            .add(button)
                                            .on_hover_text_at_pointer("Output")
                                            .clicked()
                                        {
                                            *v = Some(Selection::Output);
                                        }
                                    });

                                    let button = wankel_engine_egui::egui::ImageButton::new(
                                        connection,
                                        wankel_engine_egui::egui::Vec2::new(64.0, 64.0),
                                    )
                                    .selected(v == &Some(Selection::Connection(None)));
                                    if ui
                                        .add(button)
                                        .on_hover_text_at_pointer("Connection")
                                        .clicked()
                                    {
                                        *v = Some(Selection::Connection(None));
                                    }
                                    ui.separator();
                                    ui.vertical(|ui| {
                                        wankel_engine_egui::egui::ScrollArea::horizontal()
                                            .auto_shrink([false; 2])
                                            .show(ui, |ui| {
                                                ui.horizontal_centered(|ui| {
                                                    for i in self.factories.keys() {

                                                        ui.allocate_ui_with_layout(Vec2::new(60.0, ui.available_height())- (ui.spacing().item_spacing * 2.0), wankel_engine_egui::egui::Layout::centered_and_justified(wankel_engine_egui::egui::Direction::TopDown), |ui|
                                                            {
                                                                let text = wankel_engine_egui::egui::widgets::SelectableLabel::new(v == &Some(Selection::Component(i.clone())), i);

                                                                if ui.add(text).clicked(){
                                                                    *v = Some(Selection::Component(i.clone()));
                                                                }
                                                            });
                                                    }
                                                })
                                            });
                                    })
                                })
                            },
                        );
                    },
                    size,
                    io.events(),
                );

                ui.wants_pointer_input()
            } else {
                false
            }
        }).unwrap();

        let scale = 32.0;

        let mut needs_render = false;

        let pos = if let Some((abs_x, abs_y)) = io.input().mouse() {
            let x = (abs_x / scale) as usize;
            let y = (abs_y / scale) as usize;

            if io.input().mouse_pressed(0) && !mouse_used {
                let element_hit = self.element_lookup.get(&(x, y));

                let selection: &mut Option<Selection> = &mut self.selection.write().unwrap();
                match element_hit {
                    Some(element) => match selection {
                        &mut Some(Selection::Connection(None)) => {
                            if check_valid_connection_start(x, y, &self.element_lookup) {
                                let address = match element {
                                    Element::Input(i) => Some(Address::IO(i.clone())),
                                    Element::Component(i) => {
                                        let pos = self.components.get(i).map(|c| c.pos());
                                        let description =
                                            self.data.component(i).map(|c| c.describe());

                                        if let (Some((_, c_y)), Some(description)) =
                                            (pos, description)
                                        {
                                            let index = (y - c_y as usize).checked_sub(1);
                                            if let Some(index) = index {
                                                if let Some((output, _)) =
                                                    description.outputs.get(index)
                                                {
                                                    Some(Address::Circuit(
                                                        i.clone(),
                                                        output.clone(),
                                                    ))
                                                } else {
                                                    None
                                                }
                                            } else {
                                                None
                                            }
                                        } else {
                                            None
                                        }
                                    }
                                    _ => None,
                                };

                                if let Some(address) = address {
                                    if !self.connections.contains_key(&address) {
                                        debug!("Start Connection!");
                                        *selection = Some(Selection::Connection(Some((
                                            (x as u8, y as u8),
                                            address,
                                        ))));
                                    }
                                }
                            }
                        }
                        &mut Some(Selection::Connection(Some((start, ref address)))) => {
                            debug!("Test");
                            if check_valid_connection_end(x, y, &self.element_lookup) {
                                let target = match element {
                                    Element::Output(i) => Some(Address::IO(i.clone())),
                                    Element::Component(i) => {
                                        let pos = self.components.get(i).map(|c| c.pos());
                                        let description =
                                            self.data.component(i).map(|c| c.describe());

                                        if let (Some((_, c_y)), Some(description)) =
                                            (pos, description)
                                        {
                                            let index = (y - c_y as usize).checked_sub(1);
                                            if let Some(index) = index {
                                                if let Some((output, _)) =
                                                    description.inputs.get(index)
                                                {
                                                    Some(Address::Circuit(
                                                        i.clone(),
                                                        output.clone(),
                                                    ))
                                                } else {
                                                    None
                                                }
                                            } else {
                                                None
                                            }
                                        } else {
                                            None
                                        }
                                    }
                                    _ => None,
                                };

                                debug!("End Connection!");

                                if let Some(target) = target {
                                    let start_usize = (start.0 as usize, start.1 as usize);

                                    let result = pathfinding::prelude::astar(
                                        &start_usize,
                                        |(old_x, old_y)| {
                                            let ret: Vec<_> = [
                                                old_x.checked_sub(1).map(|x| (x, *old_y)),
                                                old_y.checked_sub(1).map(|y| (*old_x, y)),
                                                old_x.checked_add(1).map(|x| (x, *old_y)),
                                                old_y.checked_add(1).map(|y| (*old_x, y)),
                                            ]
                                            .into_iter()
                                            .filter_map(|x| x)
                                            .filter(|(try_x, try_y)| {
                                                if *try_x == x && *try_y == y {
                                                    return true;
                                                }

                                                match self.element_lookup.get(&(*try_x, *try_y)) {
                                                    Some(Element::Connection(_)) => true,
                                                    None => true,
                                                    _ => false,
                                                }
                                            })
                                            .map(|v| (v, 1))
                                            .collect();
                                            ret
                                        },
                                        |(x1, y1)| x1.abs_diff(x) + y1.abs_diff(y),
                                        |(try_x, try_y)| *try_x == x && *try_y == y,
                                    );

                                    if let Some(result) = result {
                                        create_connection(
                                            ctx,
                                            result,
                                            address,
                                            target,
                                            &self.texture_layer,
                                            &mut self.element_lookup,
                                            &mut self.factory,
                                            &mut self.data,
                                            &mut self.connections,
                                            &self.textures.connections,
                                        );

                                        *selection = Some(Selection::Connection(None));

                                        needs_render = true;
                                    }
                                }
                            }
                        }
                        _ => {
                            if let Element::Input(i) = element {
                                let val = !self.data.input(i);
                                self.data.set_input(i.clone(), val);
                                self.data.simulate();

                                needs_render = true;
                            }
                        }
                    },
                    None => {
                        match selection {
                            &mut Some(Selection::Input) => {
                                let name = format!("{}", self.inputs.len()); //TODO: use actual name
                                create_input(
                                    ctx,
                                    &self.texture_layer,
                                    &self.textures.io,
                                    &mut self.inputs,
                                    &mut self.factory,
                                    &mut self.data,
                                    &mut self.element_lookup,
                                    name,
                                    x,
                                    y,
                                );
                                needs_render = true;
                            }
                            &mut Some(Selection::Output) => {
                                let name = format!("{}", self.outputs.len()); //TODO: use actual name
                                create_output(
                                    ctx,
                                    &self.texture_layer,
                                    &self.textures.io,
                                    &mut self.outputs,
                                    &mut self.factory,
                                    &mut self.data,
                                    &mut self.element_lookup,
                                    name,
                                    x,
                                    y,
                                );
                                needs_render = true;
                            }
                            &mut Some(Selection::Component(ref c)) => {
                                let f = self.factories.get(c);
                                if let Some(f) = f {
                                    let f = f.as_ref().clone();

                                    let c = f.produce().describe();

                                    let num_inputs = c.inputs.len();
                                    let num_outputs = c.outputs.len();
                                    let height = num_inputs.max(num_outputs) + 2;

                                    let width = 3;

                                    if !self.check_element_map(x, y, width, height) {
                                        let index = self.factory.circuits.len();

                                        let element_lookup = &mut self.element_lookup;
                                        let factory = &mut self.factory;
                                        let data = &mut self.data;
                                        let components = &mut self.components;

                                        create_component(
                                            &mut ctx,
                                            x,
                                            y,
                                            width,
                                            height,
                                            index,
                                            &self.texture_layer,
                                            &self.textures.component,
                                            &self.textures.io,
                                            element_lookup,
                                            factory,
                                            f,
                                            data,
                                            components,
                                            num_inputs,
                                            num_outputs,
                                        );

                                        needs_render = true;
                                    }
                                }
                            }

                            &mut Some(Selection::Existing()) => todo!(),
                            _ => {}
                        }
                    }
                }
            }

            Some(((x, y), (abs_x, abs_y)))
        } else {
            None
        };

        for (name, (_, clock_time)) in &mut self.inputs {
            if let Some(clock_time) = clock_time {
                *clock_time += d_time;

                let mut i = self.data.input(&name);
                while *clock_time > globals.clock_timer {
                    i = !i;
                    *clock_time -= globals.clock_timer;
                }

                self.data.set_input(name.clone(), i);
                self.data.simulate();

                needs_render = true;
            }
        }

        let mut text_backdrop = None;

        if let Some((grid_pos, screen_pos)) = pos {
            let text = if let Some(elem) = self.element_lookup.get(&grid_pos) {
                match elem {
                    Element::Input(i) => Some(i),
                    Element::Output(i) => Some(i),
                    Element::Connection(Address::IO(i)) => Some(i),
                    Element::Connection(Address::Circuit(_c, i)) => Some(i),
                    Element::Component(_c) => None,
                }
            } else {
                None
            };

            needs_render |= self
                .text_layer
                .update(ctx, |layer, c| {
                    self.mouseover_text
                        .update(layer, |s| {
                            if let Some(text_str) = text {
                                if let Some(text) = s.get_mut(0) {
                                    text.set_text(text_str);
                                } else {
                                    s.add_text(Text::new(text_str, globals.font).with_scale(32.0));
                                }
                                s.set_position(Point::new(
                                    screen_pos.0 + 10.0,
                                    screen_pos.1 + 10.0,
                                ));

                                text_backdrop = s.measure(c).map(|r| r.expanded(5.0));
                                
                                s.set_disabled(false);
                                true
                            } else {
                                if !s.disabled() {
                                    s.set_disabled(true);
                                    true
                                } else {
                                    false
                                }
                            }
                        })
                        .unwrap()
                })
                .unwrap();
        }

        if needs_render {
            self.render(ctx, text_backdrop);
        }

        ctx.push_layer(None, self.text_layer);
        ctx.push_layer(Some(0.1), self.texture_layer);

        ctx.push_layer(None, self.ui);
        None
    }

    fn check_element_map(
        &self,
        x_offset: usize,
        y_offset: usize,
        width: usize,
        height: usize,
    ) -> bool {
        for x in 0..width {
            for y in 0..height {
                if self
                    .element_lookup
                    .contains_key(&(x + x_offset, y + y_offset))
                {
                    return true;
                }
            }
        }
        false
    }

    fn render_connections(&self, layer: &mut TextureAtlasLayer) {
        for (address, connection) in &self.connections {
            let active = match address {
                Address::IO(name) => self.data.input(name),
                Address::Circuit(index, name) => {
                    if let Some(c) = self.data.component(index) {
                        c.output(name)
                    } else {
                        false
                    }
                }
            };
            connection.render(
                layer,
                if active {
                    Color::GREEN
                } else {
                    Color {
                        a: 1.0,
                        r: 80.0 / 256.0,
                        g: 80.0 / 256.0,
                        b: 80.0 / 256.0,
                    }
                },
            );
        }
    }

    fn render(&self, ctx: &mut Context, text_backdrop: Option<Rect<f32>>) {
        self.texture_layer.update(ctx, |layer, _ctx| {

            self.mouseover_text_backdrop.update(layer, |t| {
                if let Some(text_backdrop) = text_backdrop{
                    let (pos, size) = text_backdrop.into_parts();
                    t.set_position(pos);
                    t.set_size(size);
                    t.enable();
                }else {
    
                    t.disable();
                }
            });
            

            self.render_connections(layer);

            self.render_components(layer);

            self.render_inputs(layer);

            self.render_outputs(layer);
        });
    }

    fn render_components(&self, layer: &mut TextureAtlasLayer) {
        for (index, component) in self.data.components() {
            if let Some(ui_component) = self.components.get(&index) {
                ui_component.render(layer, component.describe());
            }
        }
    }

    fn render_inputs(&self, layer: &mut TextureAtlasLayer) {
        for (name, value) in self.data.inputs().iter() {
            let input_pos = self.inputs.get(name);
            if let Some((handle, _)) = input_pos {
                self.render_io(layer, *handle, value);
            }
        }
    }

    fn render_outputs(&self, layer: &mut TextureAtlasLayer) {
        for (name, value) in self.data.outputs().iter() {
            let input_pos = self.outputs.get(name);
            if let Some(handle) = input_pos {
                self.render_io(layer, *handle, value);
            }
        }
    }

    fn render_io(
        &self,
        layer: &mut TextureAtlasLayer,
        handle: Handle<LayerTexture, (bool, usize)>,
        value: &bool,
    ) {
        handle
            .update(layer, |texture| {
                texture.set_tint_color(Some(if *value {
                    Color::GREEN
                } else {
                    Color {
                        a: 1.0,
                        r: 80.0 / 256.0,
                        g: 80.0 / 256.0,
                        b: 80.0 / 256.0,
                    }
                }))
            })
            .unwrap();
    }
}

fn create_connection(
    ctx: &mut Context,
    result: (Vec<(usize, usize)>, usize),
    start: &Address,
    target: Address,
    texture_layer: &Handle<TextureAtlasLayer>,
    element_lookup: &mut HashMap<(usize, usize), Element>,
    factory: &mut CompositFactory,
    data: &mut Composit,
    connections: &mut HashMap<Address, Connection>,
    textures: &ConnectionTextures,
) {
    let mut prev_cell = None;
    let mut prev_direction = None;
    let mut handles = Vec::with_capacity(result.1);

    for cell in result.0 {
        if !element_lookup.contains_key(&cell) {
            element_lookup.insert(cell, Element::Connection(start.clone()));
        }

        let direction = match (prev_cell, cell) {
            (Some((prev_x, _)), (x, _)) if prev_x < x => Some(ConnectionDirection::Right),
            (Some((prev_x, _)), (x, _)) if prev_x > x => Some(ConnectionDirection::Left),
            (Some((_, prev_y)), (_, y)) if prev_y < y => Some(ConnectionDirection::Down),
            (Some((_, prev_y)), (_, y)) if prev_y > y => Some(ConnectionDirection::Up),
            _ => None,
        };

        if let Some(texture) = get_texture(prev_direction, direction, textures) {
            handles.push((texture, prev_cell.unwrap()));
        }

        prev_direction = direction;
        prev_cell = Some(cell);
    }

    if let Some(texture) = get_texture(prev_direction, None, textures) {
        handles.push((texture, prev_cell.unwrap()));
    }

    let handles = texture_layer
        .update(ctx, |layer, _ctx| {
            let mut ret = Vec::with_capacity(handles.len());

            for ((texture, options), (x, y)) in handles.iter_mut() {
                ret.push(layer.render_texture(
                    *texture,
                    Rect::new(*x as f32 * SCALE, *y as f32 * SCALE, SCALE, SCALE),
                    options.take(),
                ));
            }

            ret
        })
        .unwrap();
    factory.connections.insert(start.clone(), vec![target]);
    connections.insert(start.clone(), Connection::new(handles));
    *data = factory.produce_composit();
}

fn get_texture(
    prev_direction: Option<ConnectionDirection>,
    direction: Option<ConnectionDirection>,
    textures: &ConnectionTextures,
) -> Option<(Handle<DynamicImage>, Option<TextureOptions>)> {
    match (prev_direction, direction) {
        (None, None) => None,
        (None, Some(d)) => match d {
            ConnectionDirection::Right => Some((
                textures.connection_end,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate180,
                    ..Default::default()
                }),
            )),
            ConnectionDirection::Left => Some((textures.connection_end, None)),
            ConnectionDirection::Up => Some((
                textures.connection_end,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate90,
                    ..Default::default()
                }),
            )),
            ConnectionDirection::Down => Some((
                textures.connection_end,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate270,
                    ..Default::default()
                }),
            )),
        },
        (Some(d), None) => match d {
            ConnectionDirection::Right => Some((textures.connection_end, None)),
            ConnectionDirection::Left => Some((
                textures.connection_end,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate180,
                    ..Default::default()
                }),
            )),
            ConnectionDirection::Up => Some((
                textures.connection_end,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate270,
                    ..Default::default()
                }),
            )),
            ConnectionDirection::Down => Some((
                textures.connection_end,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate90,
                    ..Default::default()
                }),
            )),
        },
        (Some(d_prev), Some(d)) => match (d_prev, d) {
            (ConnectionDirection::Right, ConnectionDirection::Right) => {
                Some((textures.connection, Default::default()))
            }
            (ConnectionDirection::Right, ConnectionDirection::Left) => None,
            (ConnectionDirection::Right, ConnectionDirection::Up) => {
                Some((textures.connection_bend, None))
            }
            (ConnectionDirection::Right, ConnectionDirection::Down) => Some((
                textures.connection_bend,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate270,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Left, ConnectionDirection::Right) => None,
            (ConnectionDirection::Left, ConnectionDirection::Left) => {
                Some((textures.connection, Default::default()))
            }
            (ConnectionDirection::Left, ConnectionDirection::Up) => Some((
                textures.connection_bend,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate90,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Left, ConnectionDirection::Down) => Some((
                textures.connection_bend,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate180,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Up, ConnectionDirection::Right) => Some((
                textures.connection_bend,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate180,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Up, ConnectionDirection::Left) => Some((
                textures.connection_bend,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate270,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Up, ConnectionDirection::Up) => Some((
                textures.connection,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate90,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Up, ConnectionDirection::Down) => None,
            (ConnectionDirection::Down, ConnectionDirection::Right) => Some((
                textures.connection_bend,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate90,
                    ..Default::default()
                }),
            )),
            (ConnectionDirection::Down, ConnectionDirection::Left) => {
                Some((textures.connection_bend, None))
            }
            (ConnectionDirection::Down, ConnectionDirection::Up) => None,
            (ConnectionDirection::Down, ConnectionDirection::Down) => Some((
                textures.connection,
                Some(TextureOptions {
                    rotate: TextureRotate::Rotate90,
                    ..Default::default()
                }),
            )),
        },
    }
}

fn create_component(
    ctx: &mut Context,
    x: usize,
    y: usize,
    width: usize,
    height: usize,
    index: usize,
    texture_layer: &Handle<TextureAtlasLayer>,
    nine_slice: &NineSlice,
    texture: &Handle<DynamicImage>,
    element_lookup: &mut HashMap<(usize, usize), Element>,
    factory: &mut CompositFactory,
    f: Box<dyn CircuitFactory>,
    data: &mut Composit,
    components: &mut HashMap<usize, Component>,
    num_inputs: usize,
    num_outputs: usize,
) {
    let mut in_handles = Vec::with_capacity(num_inputs);
    let mut out_handles = Vec::with_capacity(num_outputs);

    let handle = texture_layer
        .update(ctx, |layer, _ctx| {
            for i in 0..num_inputs {
                in_handles.push(layer.render_texture(
                    *texture,
                    Rect::new(
                        x as f32 * SCALE,
                        ((y + 1) as f32 + i as f32) * SCALE,
                        SCALE,
                        SCALE,
                    ),
                    None,
                ));
            }

            for i in 0..num_outputs {
                out_handles.push(layer.render_texture(
                    *texture,
                    Rect::new(
                        (x + width - 1) as f32 * SCALE,
                        ((y + 1) as f32 + i as f32) * SCALE,
                        SCALE,
                        SCALE,
                    ),
                    None,
                ));
            }

            layer.render_nine_slice(
                nine_slice,
                Rect::new(
                    SCALE * x as f32 + 10.0,
                    SCALE * y as f32 + 10.0,
                    SCALE * width as f32 - 20.0,
                    SCALE * height as f32 - 20.0,
                ),
                TextureOptions {
                    tint_color: Some(Color {
                        a: 1.0,
                        r: 30.0 / 256.0,
                        g: 67.0 / 256.0,
                        b: 126.0 / 256.0,
                    }),
                    ..Default::default()
                },
            )
        })
        .unwrap();

    for x_off in 0..width {
        for y_off in 0..height {
            element_lookup.insert((x + x_off, y + y_off), Element::Component(index));
        }
    }
    factory.circuits.push(f);
    *data = factory.produce_composit();
    components.insert(
        index,
        Component::new(
            x as u8,
            y as u8,
            width as u8,
            handle,
            in_handles,
            out_handles,
        ),
    );
}

fn create_output(
    ctx: &mut Context,
    texture_layer: &Handle<TextureAtlasLayer>,
    texture: &Handle<DynamicImage>,
    outputs: &mut HashMap<String, Handle<LayerTexture, (bool, usize)>>,
    factory: &mut CompositFactory,
    data: &mut Composit,
    element_lookup: &mut HashMap<(usize, usize), Element>,
    name: String,
    x: usize,
    y: usize,
) {
    let handle = texture_layer
        .update(ctx, |layer, _ctx| {
            layer.render_texture(
                *texture,
                Rect::new(x as f32 * SCALE, y as f32 * SCALE, SCALE, SCALE),
                None,
            )
        })
        .unwrap();
    outputs.insert(name.clone(), handle);
    factory.outputs.push(name.clone());
    *data = factory.produce_composit();

    element_lookup.insert((x, y), Element::Output(name));
}

fn create_input(
    ctx: &mut Context,
    texture_layer: &Handle<TextureAtlasLayer>,
    texture: &Handle<DynamicImage>,
    inputs: &mut HashMap<String, (Handle<LayerTexture, (bool, usize)>, Option<f32>)>,
    factory: &mut CompositFactory,
    data: &mut Composit,
    element_lookup: &mut HashMap<(usize, usize), Element>,
    name: String,
    x: usize,
    y: usize,
) {
    let handle = texture_layer
        .update(ctx, |layer, _ctx| {
            layer.render_texture(
                *texture,
                Rect::new(x as f32 * SCALE, y as f32 * SCALE, SCALE, SCALE),
                None,
            )
        })
        .unwrap();
    inputs.insert(name.clone(), (handle, None));
    factory.inputs.push(name.clone());
    *data = factory.produce_composit();
    element_lookup.insert((x, y), Element::Input(name));
}

fn check_valid_connection_start(
    x: usize,
    y: usize,
    element_lookup: &HashMap<(usize, usize), Element>,
) -> bool {
    match element_lookup.get(&(x, y)) {
        Some(Element::Input(_)) => true,
        Some(Element::Component(c)) => {
            let above = y
                .checked_sub(1)
                .map(|y| (x, y))
                .and_then(|p| element_lookup.get(&p));
            let below = y
                .checked_add(1)
                .map(|y| (x, y))
                .and_then(|p| element_lookup.get(&p));
            let right = x
                .checked_add(1)
                .map(|x| (x, y))
                .and_then(|p| element_lookup.get(&p));

            if let Some(Element::Component(c2)) = above {
                if c != c2 {
                    return false;
                }
            }

            if let Some(Element::Component(c2)) = below {
                if c != c2 {
                    return false;
                }
            }

            match right {
                None => true,
                Some(Element::Connection(_)) => true,
                _ => false,
            }
        }
        _ => false,
    }
}

fn check_valid_connection_end(
    x: usize,
    y: usize,
    element_lookup: &HashMap<(usize, usize), Element>,
) -> bool {
    match element_lookup.get(&(x, y)) {
        Some(Element::Output(_)) => true,
        Some(Element::Component(c)) => {
            let above = y
                .checked_sub(1)
                .map(|y| (x, y))
                .and_then(|p| element_lookup.get(&p));
            let below = y
                .checked_add(1)
                .map(|y| (x, y))
                .and_then(|p| element_lookup.get(&p));
            let left = x
                .checked_sub(1)
                .map(|x| (x, y))
                .and_then(|p| element_lookup.get(&p));

            if let Some(Element::Component(c2)) = above {
                if c != c2 {
                    return false;
                }
            }

            if let Some(Element::Component(c2)) = below {
                if c != c2 {
                    return false;
                }
            }

            match left {
                None => true,
                Some(Element::Connection(_)) => true,
                _ => false,
            }
        }
        _ => false,
    }
}

#[derive(Debug, Default)]
struct And {
    a: bool,
    b: bool,
    simulation_steps: VecDeque<SimulationStep>,
}

impl Circuit for And {
    fn set_input(&mut self, index: String, value: bool) {
        match index.as_str() {
            "a" => {
                let a = self.a;
                self.a = value;

                if (self.a && self.b) != (a && self.b) {
                    self.simulation_steps.push_back(SimulationStep::Connection(
                        Address::IO("a".to_string()),
                        Address::IO("output".to_string()),
                    ));
                }
            }
            "b" => {
                let b = self.b;
                self.b = value;
                if (self.a && self.b) != (self.a && b) {
                    self.simulation_steps.push_back(SimulationStep::Connection(
                        Address::IO("b".to_string()),
                        Address::IO("output".to_string()),
                    ));
                }
            }
            _ => {}
        }
    }

    fn output(&self, index: &String) -> bool {
        match index.as_str() {
            "output" => self.a && self.b,
            _ => false,
        }
    }

    fn simulate_step(&mut self) -> Option<SimulationStep> {
        self.simulation_steps.pop_front()
    }

    fn describe(&self) -> Description {
        Description {
            inputs: vec![("a".to_string(), self.a), ("b".to_string(), self.b)],
            outputs: vec![("output".to_string(), self.a && self.b)],
            name: "And".to_string(),
        }
    }

    fn apply(&mut self, step: SimulationStep) -> Option<String> {
        if let SimulationStep::Connection(_, Address::IO(name)) = step {
            Some(name)
        } else {
            None
        }
    }

    fn input(&self, index: &String) -> bool {
        match index.as_str() {
            "a" => self.a,
            "b" => self.b,
            _ => false,
        }
    }
}

#[derive(Debug, Clone)]
struct AndFactory;

impl CircuitFactory for AndFactory {
    fn produce(&self) -> Box<dyn Circuit> {
        Box::new(And::default())
    }

    fn clone(&self) -> Box<dyn CircuitFactory> {
        Box::new(Clone::clone(self))
    }
}

#[derive(Debug, Default)]
struct Not {
    a: bool,
    simulation_steps: VecDeque<SimulationStep>,
}

impl Circuit for Not {
    fn set_input(&mut self, index: String, value: bool) {
        match index.as_str() {
            "a" => {
                self.a = value;
                self.simulation_steps.push_back(SimulationStep::Connection(
                    Address::IO("a".to_string()),
                    Address::IO("output".to_string()),
                ));
            }
            _ => {}
        }
    }

    fn output(&self, index: &String) -> bool {
        match index.as_str() {
            "output" => !self.a,
            _ => false,
        }
    }

    fn simulate_step(&mut self) -> Option<SimulationStep> {
        self.simulation_steps.pop_front()
    }

    fn describe(&self) -> Description {
        Description {
            inputs: vec![("a".to_string(), self.a)],
            outputs: vec![("output".to_string(), !self.a)],
            name: "Not".to_string(),
        }
    }

    fn apply(&mut self, step: SimulationStep) -> Option<String> {
        if let SimulationStep::Connection(_, Address::IO(name)) = step {
            Some(name)
        } else {
            None
        }
    }

    fn input(&self, index: &String) -> bool {
        match index.as_str() {
            "a" => self.a,
            _ => false,
        }
    }
}

#[derive(Debug, Clone)]
struct NotFactory;

impl CircuitFactory for NotFactory {
    fn produce(&self) -> Box<dyn Circuit> {
        Box::new(Not::default())
    }

    fn clone(&self) -> Box<dyn CircuitFactory> {
        Box::new(Clone::clone(self))
    }
}
