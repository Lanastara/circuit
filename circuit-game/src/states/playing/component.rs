use wankel_engine::{
    layer::{
        text::prelude::*,
        texture_atlas::{LayerTexture, NineSliceHandle},
    },
    prelude::*,
};

use super::Textures;

#[derive(Debug, Clone)]
pub(crate) struct Component {
    pub(crate) x: u8,
    pub(crate) y: u8,
    pub(crate) width: u8,
    pub(crate) handle: NineSliceHandle,
    pub(crate) input_handles: Vec<Handle<LayerTexture, (bool, usize)>>,
    pub(crate) output_handles: Vec<Handle<LayerTexture, (bool, usize)>>,
}

impl Component {
    pub(crate) fn new(
        x: u8,
        y: u8,
        width: u8,
        handle: NineSliceHandle,
        input_handles: Vec<Handle<LayerTexture, (bool, usize)>>,
        output_handles: Vec<Handle<LayerTexture, (bool, usize)>>,
    ) -> Self {
        Self {
            x,
            y,
            width,
            handle,
            input_handles,
            output_handles,
        }
    }

    pub(crate) fn pos(&self) -> (u8, u8) {
        (self.x, self.y)
    }

    pub(crate) fn render(&self, layer: &mut TextureAtlasLayer, description: circuit::Description) {
        for ((_name, state), handle) in description.inputs.iter().zip(self.input_handles.iter()) {
            handle
                .update(layer, |texture| {
                    texture.set_tint_color(Some(if *state {
                        Color::GREEN
                    } else {
                        Color {
                            a: 1.0,
                            r: 80.0 / 256.0,
                            g: 80.0 / 256.0,
                            b: 80.0 / 256.0,
                        }
                    }))
                })
                .unwrap();
        }

        for ((_name, state), handle) in description.outputs.iter().zip(self.output_handles.iter()) {
            handle
                .update(layer, |texture| {
                    texture.set_tint_color(Some(if *state {
                        Color::GREEN
                    } else {
                        Color {
                            a: 1.0,
                            r: 80.0 / 256.0,
                            g: 80.0 / 256.0,
                            b: 80.0 / 256.0,
                        }
                    }))
                })
                .unwrap();
        }
    }
}
