use wankel_engine::{layer::texture_atlas::LayerTexture, prelude::*};

#[derive(Debug, Clone, Copy, PartialEq)]
pub(crate) enum ConnectionDirection {
    Right,
    Left,
    Up,
    Down,
}

#[derive(Debug, Clone)]
pub(crate) struct Connection {
    handles: Vec<Handle<LayerTexture, (bool, usize)>>,
}

impl Connection {
    pub(crate) fn new(handles: Vec<Handle<LayerTexture, (bool, usize)>>) -> Connection {
        Connection { handles }
    }

    pub(crate) fn render(&self, layer: &mut TextureAtlasLayer, color: impl Into<Option<Color>>) {
        let color = color.into();

        for h in &self.handles {
            h.update(layer, |texture| {
                texture.set_tint_color(color.clone());
            });
        }
    }
}

#[derive(Debug, Clone)]
pub(crate) struct ConnectionTextures {
    pub connection: Handle<DynamicImage>,
    pub connection_bend: Handle<DynamicImage>,
    pub connection_3: Handle<DynamicImage>,
    pub connection_4: Handle<DynamicImage>,
    pub _connection_over: Handle<DynamicImage>,
    pub connection_end: Handle<DynamicImage>,
}
impl ConnectionTextures {
    pub fn load(ctx: &mut wankel_engine::Context, base_path: &str) -> Self {
        ConnectionTextures {
            connection: ctx
                .load_asset::<DynamicImage>(&format!("{}/connection/straight.png", base_path)),
            connection_bend: ctx
                .load_asset::<DynamicImage>(&format!("{}/connection/bend.png", base_path)),
            connection_3: ctx
                .load_asset::<DynamicImage>(&format!("{}/connection/3.png", base_path)),
            connection_4: ctx
                .load_asset::<DynamicImage>(&format!("{}/connection/4.png", base_path)),
            _connection_over: ctx
                .load_asset::<DynamicImage>(&format!("{}/connection/over.png", base_path)),
            connection_end: ctx
                .load_asset::<DynamicImage>(&format!("{}/connection/end.png", base_path)),
        }
    }

    pub(crate) fn add_textures(
        &self,
        builder: &mut wankel_engine::texture::atlas::UniformTextureAtlasBuilder,
    ) {
        builder
            .add_texture(self.connection)
            .add_texture(self.connection_3)
            .add_texture(self.connection_4)
            .add_texture(self.connection_bend)
            .add_texture(self.connection_end);
    }
}
